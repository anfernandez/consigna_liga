require 'byebug'

Dado('hay {int} equipos registrados') do |cantidad_de_equipo|
  (0..(cantidad_de_equipo - 1)).each do |x|
    body = {
      'nombre_equipo': "EQUIPO-#{x}",
      'presupuesto': 300
    }
    post '/equipos', body.to_json, 'CONTENT_TYPE' => 'application/json'
  end
end

Cuando('registro un equipo {string} con presupuesto {int}') do |nombre_equipo, presupuesto|
  body = {
    'nombre_equipo': nombre_equipo,
    'presupuesto': presupuesto
  }
  post '/equipos', body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Entonces('se registra correctamente') do
  expect(last_response.status).to be == 201
  @id_equipo = JSON.parse(last_response.body)['id']
  expect(@id_equipo).not_to be_nil
end

Entonces('obtengo un error') do
  expect(last_response.status).to be == 400
  error = JSON.parse(last_response.body)['error']
  expect(error).not_to be_nil
end

Entonces('obtengo un error por exceso de equipos') do
  expect(last_response.status).to be == 400
  error = JSON.parse(last_response.body)['error']
  expect(error).to eq 'error_liga_completa'
end

Entonces('obtengo un error por exceso de presupuesto') do
  expect(last_response.status).to be == 400
  error = JSON.parse(last_response.body)['error']
  expect(error).to eq 'error_presupuesto_completo'
end
